LIVE_SHARDS = {
    '401000001': 'Sillyville',
    '402000001': 'Nutty River',
    '403000001': 'Zany Acres'
}

TEST_SHARDS = {
    '401000001': 'TestTown 1',
    '402000001': 'TestTown 2',
    '403000000': 'ToonValley'
}

OTP_ENTERING_SHARD = 'OTPClientRepository: Entering shard'
OTP_LOADING_DNA = 'dna: Reading /c/Program Files (x86)/Disney/Disney Online/ToontownOnline/'
READING_LOG = 'Using log file: {0}'
COMMAND_SPACE = ' '
NEW_SHARD = 'Shard with identifier {0} entered with name of {1}.'

DNA_TO_NAME = {
    'phase_4/dna/storage.dna': 'Loading',
    'phase_3.5/dna/storage_interior.dna': 'Loading',
    'phase_5/dna/storage_town.dna': 'Loading',

    'mods/TL_Cookies/toonland/playground/OutdoorZone/dna/outdoor_zone_sz.dna': "Chip 'n Dale's Acorn Acres",
    'mods/TL_Cookies/toonland/playground/OutdoorZone/dna/storage_OZ_sz.dna': "Chip 'n Dale's Acorn Acres",
    'mods/TL_Cookies/toonland/playground/OutdoorZone/dna/storage_OZ_town.dna': "Chip 'n Dale's Acorn Acres",
    'mods/TL_Cookies/toonland/playground/OutdoorZone/dna/outdoor_zone_6100.dna': "Chip 'n Dale's Acorn Acres (Nutty Place)",

    'phase_6/dna/storage_OZ.dna': "Chip 'n Dale's Acorn Acres",
    'phase_6/dna/storage_OZ_sz.dna': "Chip 'n Dale's Acorn Acres",
    'phase_6/dna/outdoor_zone_sz.dna': "Chip 'n Dale's Acorn Acres",

    'phase_4/dna/toontown_central_sz.dna': 'Toontown Central',
    'phase_4/dna/storage_TT.dna': 'Toontown Central',
    'phase_4/dna/storage_TT_sz.dna': 'Toontown Central',
    'phase_4/dna/winter_storage_TT.dna': 'Toontown Central',
    'phase_4/dna/winter_storage_TT_sz.dna': 'Toontown Central',
    'phase_4/dna/halloween_props_storage_TT.dna': 'Toontown Central',
    'phase_4/dna/halloween_props_storage_TT_sz.dna': 'Toontown Central',
    'phase_5/dna/storage_TT_town.dna': 'Toontown Central',
    'phase_5/dna/toontown_central_2300.dna': 'Toontown Central (Punchline Place)',
    'phase_5/dna/toontown_central_2200.dna': 'Toontown Central (Loopy Lane)',

    'phase_6/dna/storage_DD.dna': "Donald's Dock",
    'phase_6/dna/storage_DD_sz.dna': "Donald's Dock",
    'phase_6/dna/donalds_dock_sz.dna': "Donald's Dock",

    'phase_8/dna/storage_BR.dna': 'The Brrrgh',
    'phase_8/dna/storage_BR_sz.dna': 'The Brrrgh',
    'phase_8/dna/the_burrrgh_sz.dna': 'The Brrrgh',
    'phase_8/dna/storage_BR_town.dna': 'The Brrrgh',
    'phase_8/dna/the_burrrgh_3200.dna': 'The Brrrgh - Sleet Street',

    'phase_8/dna/storage_DG.dna': "Daisy's Garden",
    'phase_8/dna/storage_DG_sz.dna': "Daisy's Garden",

    'phase_5.5/dna/storage_estate.dna': 'Estate',
    'phase_5.5/dna/estate_1.dna': 'Estate',

    'phase_9/dna/cog_hq_sellbot_sz.dna': 'Sellbot HQ',

    'phase_13/dna/storage_party_sz.dna': 'Party',
    'phase_13/dna/party_sz.dna': 'Party',

    'phase_6/dna/storage_MM.dna': "Minnie's Melodyland",
    'phase_6/dna/storage_MM_sz.dna': "Minnie's Melodyland",
    'phase_6/dna/minnies_melody_land_sz.dna': "Minnie's Melodyland",
    'phase_6/dna/storage_MM_town.dna': "Minnie's Melodyland",
    'phase_6/dna/minnies_melody_land_4100.dna': "Minnie's Melodyland - Alto Avenue",

    'phase_6/dna/storage_GZ.dna': "Chip 'n Dale's MiniGolf",
    'phase_6/dna/storage_GZ_sz.dna': "Chip 'n Dale's MiniGolf",
    'phase_6/dna/golf_zone_sz.dna': "Chip 'n Dale's MiniGolf",

    'phase_8/dna/storage_DL.dna': "Donald's Dreamland",

    'phase_10/dna/cog_hq_cashbot_sz.dna': 'Cashbot HQ'
}