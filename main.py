from pypresence import Presence
from threading import Thread
import constants, time, glob, os, sys

rpcClient = Presence('592845025331642389')
rpcClient.connect()

startTime = int(time.time())
isTest = '--test' in sys.argv

def scanLogFile():
    clientType = 'ToontownOnline_TEST' if isTest else 'ToontownOnline'

    directory = 'C:/Program Files (x86)/Disney/Disney Online/{0}'.format(clientType)

    newest = max(glob.iglob(directory + '/*.log'), key = os.path.getctime)
    print(constants.READING_LOG.format(newest))

    logFile = open(newest)

    logLines = followLog(logFile)

    haveZone = False
    shardName = 'Unknown Shard'

    for line in logLines:
        if constants.OTP_ENTERING_SHARD in line:
            shardLine = line.split(constants.COMMAND_SPACE)
            shardId = shardLine[-1].rstrip()

            if not isTest:
                shardName = constants.LIVE_SHARDS[shardId]
            else:
                shardName = constants.TEST_SHARDS[shardId]

            print(constants.NEW_SHARD.format(int(shardId), shardName))

        if constants.OTP_LOADING_DNA in line:
            dnaFile = line.split(constants.COMMAND_SPACE)[-1].rstrip()[22:]
            print('DNA', dnaFile)

            zoneName = getZoneName(dnaFile)
            haveZone = True

        if haveZone and shardName != 'Unknown Shard':
            # Send our data.
            data = {
                'details': f"Disney's Toontown Online ({shardName})",
                'state': zoneName,
                'start': startTime,
                'large_image': 'sunrise_games',
                'large_text': 'Sunrise Games'
            }

            updatePresence(data)

def getZoneName(dnaFile):
    zoneName = constants.DNA_TO_NAME[dnaFile]
    return zoneName

def updatePresence(data):
    # Send the update to the RPC client.
    buttons = [
        {
            'label': 'Website', 'url': 'https://sunrise.games'
        },
        {
          'label': 'Discord', 'url': 'https://discord.gg/3M5SwEybWW'
        }
    ]

    rpcClient.update(**data, buttons = buttons)

def followLog(logFile):
    logFile.seek(0, 2)

    while True:
        line = logFile.readline()

        if not line:
            time.sleep(0.1)
            continue

        yield line

thread = Thread(target = scanLogFile)
thread.start()